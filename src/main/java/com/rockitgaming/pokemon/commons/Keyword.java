package com.rockitgaming.pokemon.commons;

/**
 * Created by dangn on 7/24/16.
 */
public enum Keyword {

    ALL, AND, OR
}
